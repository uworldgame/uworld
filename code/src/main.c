#include <stdio.h>
#include <string.h>

#include <placeholder_lib.h>

int main(){
  printf("Hello uWorld!\n");
  printf("uWorld version: %s\n", UWORLD_VERSION);

  printf("Placeholder: %d\n", placeholder_function(210));
}
